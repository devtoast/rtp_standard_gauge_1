function data()
	return {
		en = {
			["Name"] = ("RTP - Tramway Standard-Gauge Pack"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nDifferent tramways in standard-gauge format."),
		},
        de = {
			["Name"] = ("RTP - Tramgleis Normalspur-Paket"),
			["Description"] = ("Roads´n Trams Project (RTP):\n\nEine vielzahl an verschiedene Tramgleise (Normalspur-Format) für den Schönbau."),
		},
	}
end
