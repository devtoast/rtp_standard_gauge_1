function data()
return {
  info = {
    minorVersion = 0,
    severityAdd = "NONE",
    severityRemove = "WARNING",
    name = _("Name"),
    description = _("Description"),
	tags = { "Tram","Street",},
	visible = true,
    authors = {
      {
		name = "easybronko",
		role = "Model & Scrip",
      },
      {
		name = "wicked1133",
		role = "UI & Scrip",
      },
      {
		name = "Yoshi",
		role = "Textures",
      },
      {
		name = "Melectro",
		role = "Textures",
      },
    },
  },
}
end